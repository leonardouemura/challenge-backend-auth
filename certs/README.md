# gRPC TLS certificate file generation

Here's a basic outline of the steps you need to take to generate TLS credentials for your gRPC server and client:

Generate TLS Certificates:

1.  Generate Certificate Authority (CA): If you don't have one already, you can create a self-signed CA certificate.

2.  Create Server Certificate: Generate a certificate signed by the CA for your gRPC server.

3.  Create Client Certificate: Generate a certificate signed by the same CA for your gRPC client.

Example Steps (Command Line):

### Generate CA certificate and key:

    openssl genrsa -passout pass:changeit -des3 -out ca.key 4096
    openssl req -passin pass:changeit -new -x509 -days 365 -key ca.key -out ca.crt

### Generate server certificate and key:

    openssl genrsa -out server.key 2048
    openssl req -new -key server.key -out server.csr
    openssl x509 -req -days 365 -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt

### Using the Certificates:

    You'll use server.crt and server.key on your gRPC server side.
    You'll use server.crt on your gRPC client side.

## For server testing purposes allowing localhost

    openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 \
    -subj "/C=BR/ST=São Paulo/L=São Paulo/O=DEV/CN=localhost" \
    -keyout server.key -out server.crt \
    -extensions san \
    -config <(echo "[req]";
              echo distinguished_name=req;
              echo "[san]";
              echo subjectAltName=DNS:localhost,IP:127.0.0.1)
