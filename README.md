# challenge-backend-auth

Ola!

Neste desafio foi proposto o fornecimento de um sistema seguro, robusto e muito performático.

Por isso escolhi desenvolver uma arquitetura baseada em microsserviços composto dos seguintes módulos:

1. ser-auth: _api para autenticação e servidor grpc para autorização_

2. ser-name: _api para cadastro de dívidas_

3. ser-score: _api para consulta de pontuação_

### Ideia Geral

Todos os microsserviços utilizam Gin para a criação da API, com GORM para orm utilizando banco de dados relacional PostgreSQL. Essa orm permite que as consultas sejam mais seguras pois bloqueiam ataques de sql injection e é prático por realizar migrações automáticas.

Para trazer segurança aos acessos dos usuários, foi implementado uma autenticação utilizando tokens JWT de curta duração junto a um hash de sessão de acesso de duração longa. O hash de sessão é armazenado no Redis com TTL equivalente ao tempo de sessão. Dessa forma, quando o tempo de sessão se encerra, a chave é deletada do Redis e o token de acesso se torna inválido.

A responsabilidade de autorização entre os serviços foi implementada no middleware gin de cada microserviço, que consulta o servidor gRPC de autorização para realizar a validação to token JWT a cada acesso. Adicionei certificado TLS para uma maior segurança de acesso. A escolha da utilização do gRPC ao inves de metodos HTTP ou AMQP, deu-se devido a performance da comunicação com o custo de menor resiliencia a falhas.

Outra medida de segurança é a separação dos bancos utilizados em cada serviço e a utilização de um controle de acesso semelhante a um RBAC.

### Requirements

    Go 1.21.3 ou superior

    PostgreSQL 14.9

    Redis 7.2

### Como rodar

1. Manualmente:

   cd ./ser-auth && make run-api

   cd ./ser-auth && make run-grpc

   cd ./ser-name && make run-api

   cd ./ser-score && make run-api

   OBS: caso necessário é possível adicionar um arquivo .env

2. Docker:

   docker build --tag challenge .

   -> rode serviço por serviço e inclua as portas corretas

   docker run --rm -it challenge ./build/ser-auth-api

   -> ou rode com docker compose

   docker compose up

### Altere a ENV ADMIN_EMAIL para o domínio desejado

a env ADMIN_EMAIL é a responsável por decidir se o usuário terá privilegios de administrador.

### Implementações futuras

- API Gateway;
- Add um gocron que realize a média da dívida e armazene-a. Isso deve reduzir a carga do banco;
- Configurar o CORS corretamente;
- Implementar paginação caso existam muitas dívidas
