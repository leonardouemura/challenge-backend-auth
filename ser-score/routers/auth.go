package routers

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	pb "gitlab.com/leonardouemura/challenge-backend-auth/rpc_auth"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-score/config"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func Authentication(c *gin.Context) {
	conf := config.LoadConfig()

	reqToken := c.Request.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer ")
	token := splitToken[1]

	// // Load the server certificate for TLS
	// serverCert, err := os.ReadFile("./server.crt")
	// if err != nil {
	// 	c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Failed to read server certificate"})
	// 	return
	// }

	// // Create a new TLS credentials using the server certificate
	// certPool := x509.NewCertPool()
	// certPool.AppendCertsFromPEM(serverCert)
	// transportCreds := credentials.NewTLS(&tls.Config{
	// 	RootCAs: certPool,
	// })

	// conn, err := grpc.Dial("localhost:8080", grpc.WithTransportCredentials(transportCreds))
	conn, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.GRPCHost, conf.GRPCPort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Could not connect to authorization server"})
	}
	defer conn.Close()

	client := pb.NewValidateTokenServiceClient(conn)

	// This is available to us through the auto-generated code
	validation, err := client.Validate(context.Background(), &pb.ValidateTokenRequest{
		Token: token,
	})
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}
	c.Set("validation", validation)
	// Continue with the chain to handler
	c.Next()
}

func CheckToken(c *gin.Context) {
	validation := c.MustGet("validation").(*pb.ValidateTokenResponse)
	c.JSON(http.StatusOK, gin.H{"ok": validation})
}
