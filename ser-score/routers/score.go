package routers

import (
	"log"
	"math"
	"net/http"

	"github.com/gin-gonic/gin"
	pb "gitlab.com/leonardouemura/challenge-backend-auth/rpc_auth"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-name/filters"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-name/models"
	"gorm.io/gorm"
)

func calculateScore(x float64) float64 {
	// Checking for negative values of x
	if x < -100 {
		log.Println("Error: Cannot take the square root of a negative number.")
		return 0.0
	}

	// Computing the result
	result := 10000 / math.Sqrt(x+100)
	return result
}
func GetScore(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	validation := c.MustGet("validation").(*pb.ValidateTokenResponse)

	var debtFilter filters.DebtFilter
	if err := c.ShouldBindQuery(&debtFilter); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var n float64
	db.Model(&models.Debt{}).Select("avg(value)").Where("cpf = ?", validation.CPF).Scan(&n)

	// Set response
	c.JSON(http.StatusOK, gin.H{"score": calculateScore(n)})
}
