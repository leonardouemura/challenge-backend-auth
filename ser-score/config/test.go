// Package config configures the Application using .env file, environment variables
// and other necessary configs
package config

// TestConfig configures the application to run int TEST mode
var TestConfig = &Config{
	GinPort:    "5002",
	BinaryName: "ser-score-api",
	LogLevel:   "DEBUG",
	DBName:     "interview_name",
	DBHost:     "localhost",
	DBPort:     "5432",
	DBUser:     "interview",
	DBPassword: "interview",
	GRPCHost:   "localhost",
	GRPCPort:   "8080",
}
