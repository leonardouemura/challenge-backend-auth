// Package config configures the Application using .env file, environment variables
// and other necessary configs
package config

// DevConfig configures the application to run int DEVELOPMENT mode
var DevConfig = &Config{
	GinPort:    "5002",
	BinaryName: "ser-score-api",
	LogLevel:   "DEBUG",
	DBName:     "interview_name",
	DBHost:     "localhost",
	DBPort:     "5432",
	DBUser:     "interview",
	DBPassword: "interview",
	GRPCHost:   "localhost",
	GRPCPort:   "8080",
}
