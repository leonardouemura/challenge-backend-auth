package info

import (
	"fmt"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

// App versions
var version = "Dev"
var commit = "Dev"
var built = "Dev"

// Info struct with info values
type Info struct {
	Version    string `json:"version" example:"1"`
	CommitHash string `json:"commit_hash" example:"2"`
	BuildTime  string `json:"build_time" example:"345"`
}

// GetBuildTime return BuildTime as int
func (i *Info) GetBuildTime() int {
	val, err := strconv.Atoi(i.BuildTime)
	if err != nil {
		return 0
	}
	return val
}

// Major verify if Version string has at least length = 1 (vX) and
// return Major version as int
func (i *Info) Major() int {
	versions := strings.Split(i.Version, ".")
	if len(versions) < 1 {
		log.Errorf("Version %v has length lower than one", i.Version)
		return 0
	}
	conv, err := strconv.Atoi(versions[0])
	if err != nil {
		log.Errorf("Error converting major %v to int: %v", versions[0], err)
		return 0
	}
	return conv
}

// Minor verify if Version string has at least length == 2 (vX.X) and
// return Major version as int
func (i *Info) Minor() int {
	versions := strings.Split(i.Version, ".")
	if len(versions) < 2 {
		log.Errorf("Version %v has length lower than two", i.Version)
		return 0
	}
	conv, err := strconv.Atoi(versions[1])
	if err != nil {
		log.Errorf("Error converting minor %v to int: %v", versions[1], err)
		return 0
	}
	return conv
}

// Minor verify if Version string has at least length == 3 (vX.X.X) and
// return Major version as int
func (i *Info) Patch() int {
	versions := strings.Split(i.Version, ".")
	if len(versions) < 3 {
		log.Errorf("Version %v has length lower than three", i.Version)
		return 0
	}
	conv, err := strconv.Atoi(versions[2])
	if err != nil {
		log.Errorf("Error converting patch %v to int: %v", versions[2], err)
		return 0
	}
	return conv
}

// PrintVersion print app info as version, built time, commit hash
func PrintVersion() {
	fmt.Printf("Version: %s\n", version)
	fmt.Printf("Build Time: %s\n", built)
	fmt.Printf("Commit Hash: %s\n", commit)
}

// GetInfo returns a struct composed of Info{Version CommitHash BuildTime}
func GetInfo() Info {
	i := Info{Version: version, CommitHash: commit, BuildTime: built}
	return i
}

// GetVersion returns app version
func (i *Info) GetVersion() string {
	return version
}
