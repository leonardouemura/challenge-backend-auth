package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"time"

	pb "gitlab.com/leonardouemura/challenge-backend-auth/rpc_auth"

	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/config"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/jwt"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/models"

	"github.com/go-redis/redis/v8"
	"google.golang.org/grpc"
)

type server struct {
	pb.UnimplementedValidateTokenServiceServer
	rd *redis.Client
}

func (s *server) Validate(ctx context.Context, in *pb.ValidateTokenRequest) (*pb.ValidateTokenResponse, error) {
	claims, err := jwt.ParseToken(in.Token)
	if err != nil {
		return nil, fmt.Errorf("error jwt.ParseToken: %s", err.Error())
	}
	// valida se a sessão
	session, err := jwt.RetrieveRefreshToken(ctx, s.rd, claims.Id)
	if err != nil {
		return nil, fmt.Errorf("error jwt.RetrieveRefreshToken: %s", err.Error())

	}
	if session.Role != claims.Role || time.Now().After(session.ExpiresAt) {
		return nil, fmt.Errorf("error: %s", "invalid session. Please login again")

	}
	response := &pb.ValidateTokenResponse{
		Id:        claims.Id,
		Role:      claims.Role,
		CPF:       session.CPF,
		ExpiresAt: claims.ExpiresAt,
	}
	return response, nil
}

func main() {
	conf := config.LoadConfig()
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%s", conf.GRPCHost, conf.GRPCPort))
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	rd := models.SetupRedis(ctx, conf)

	s := grpc.NewServer()
	pb.RegisterValidateTokenServiceServer(s, &server{rd: rd})
	go func() {
		if err := s.Serve(listener); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()
	<-c
	cancel()

	// THIS CODE WORKS BUT I NEED TO FIGURE IT OUT HOW TO PROPERLY CREATE A CERT TO RUN IN DOCKER COMPOSE
	// // Load server certificate and private key
	// serverCert, err := os.ReadFile("./server.crt")
	// if err != nil {
	// 	log.Fatalf("failed to read server certificate: %v", err)
	// }
	// serverKey, err := os.ReadFile("./server.key")
	// if err != nil {
	// 	log.Fatalf("failed to read server private key: %v", err)
	// }

	// // Create a new TLS credentials using the certificate and key
	// certificate, err := tls.X509KeyPair(serverCert, serverKey)
	// if err != nil {
	// 	log.Fatalf("failed to load server TLS credentials: %v", err)
	// }

	// // Create a new gRPC server with TLS credentials
	// tlsConfig := &tls.Config{
	// 	Certificates: []tls.Certificate{certificate},
	// }
	// s := grpc.NewServer(grpc.Creds(credentials.NewTLS(tlsConfig)))

	// // Create a listener with TLS
	// listener, err := net.Listen("tcp", ":8080")
	// if err != nil {
	// 	log.Fatalf("failed to listen: %v", err)
	// }

	// // Graceful shutdown
	// ctx, cancel := context.WithCancel(context.Background())
	// defer cancel()
	// go func() {
	// 	c := make(chan os.Signal, 1)
	// 	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	// 	<-c
	// 	s.GracefulStop()
	// }()

	// // Setup Redis
	// rd := models.SetupRedis(ctx, conf)

	// // Register gRPC server
	// pb.RegisterValidateTokenServiceServer(s, &server{rd: rd})

	// // Start serving
	// if err := s.Serve(listener); err != nil {
	// 	log.Fatalf("failed to serve: %v", err)
	// }
}
