package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/config"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/info"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/models"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/routers"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

// setupLog configures log output level with "INFO" as default
func setupLog(conf *config.Config) {
	executable, _ := os.Executable()
	executable = filepath.FromSlash(executable)
	directory := filepath.Dir(executable)
	log.SetFormatter(&log.TextFormatter{
		DisableQuote: true,
	})
	logLevel := strings.ToUpper(conf.LogLevel)
	if logLevel == "DEBUG" {
		log.SetLevel(log.DebugLevel)

	} else if logLevel == "ERROR" {
		log.SetLevel(log.ErrorLevel)

	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Infof("Executable: %v", executable)
	log.Infof("Dir: %v", directory)
	log.Infof("Log Level: %v", log.GetLevel())
}

func main() {
	conf := config.LoadConfig()

	showVersion := flag.Bool("version", false, "Show version info of the binary")

	flag.Parse()

	if *showVersion {
		info.PrintVersion()
		return
	}

	run(conf)
}

func run(conf *config.Config) {
	setupLog(conf)

	// Run server
	appInfo := info.GetInfo()
	log.Info("**** Authentication")
	log.Infof("Version: %v", appInfo.Version)
	log.Infof("Build: %v", appInfo.BuildTime)
	log.Infof("Commit: %v", appInfo.CommitHash)
	log.Infof("CPU: %v", runtime.GOARCH)
	log.Infof("Platform: %v", runtime.GOOS)

	// Create router
	r := gin.Default()

	ctx, cancel := context.WithCancel(context.Background())
	// Create models
	db, rd := models.SetupModels(ctx, conf)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Configure CORS middleware
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PATCH", "DELETE"},
		AllowHeaders:     []string{"Origin", "Authorization", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
	}))

	r.Use(func(c *gin.Context) {
		c.Set("db", db)
		c.Set("rd", rd)
		c.Next()
	})

	// Endpoints configuration
	// r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.POST("/api/v1/auth/login", routers.Login)
	r.POST("/api/v1/auth/users", routers.CreateUser)
	r.GET("/api/v1/auth/refresh/:id", routers.RefreshToken)
	api := r.Group("/api/v1/auth").Use(routers.Authentication)
	{
		routers.GetEndpoints(api)
	}

	go func() {
		if err := r.Run(fmt.Sprintf(":%v", conf.GinPort)); err != nil {
			log.Fatal(err)
		}
	}()

	<-c
	cancel()
}
