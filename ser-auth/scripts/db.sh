#!/usr/bin/env bash
set -e

sudo -u postgres psql -c 'CREATE DATABASE interview_auth;'
sudo -u postgres psql -c "CREATE ROLE interview WITH SUPERUSER LOGIN PASSWORD 'interview';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE interview_auth TO interview;"