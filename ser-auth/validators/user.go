package validators

import (
	"fmt"
	"regexp"

	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/models"
)

func ValidateUser(user models.User) error {
	if err := ValidateCPF(user.CPF); err != nil {
		return err
	}
	if err := ValidateBirth(user.BirthDate); err != nil {
		return err
	}
	if err := ValidateName(user.Name); err != nil {
		return err
	}
	return nil
}

func ValidateCPF(cpf string) error {
	pattern := `^\d{3}\.\d{3}\.\d{3}-\d{2}$`
	if match, _ := regexp.MatchString(pattern, cpf); match {
		return nil
	} else {
		return fmt.Errorf("error: invalid cpf format. Use the following: xxx.xxx.xxx-xx")
	}
}

func ValidateBirth(birth string) error {
	pattern := `^(?:0[1-9]|[12]\d|3[01])([\/.-])(?:0[1-9]|1[012])\/(?:19|20)\d\d$`
	if match, _ := regexp.MatchString(pattern, birth); match {
		return nil
	} else {
		return fmt.Errorf("error: invalid birth format. Use the following: dd/mm/yyyy")
	}
}

func ValidateName(name string) error {
	if len(name) == 0 {
		return fmt.Errorf("error: please enter a name")
	}
	return nil
}
