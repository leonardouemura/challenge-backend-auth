package validators

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/models"
)

func TestValidateUser(t *testing.T) {
	t.Run("invalid cpf format", func(t *testing.T) {
		user := models.User{
			CPF:       "454454454984",
			Name:      "Someone X",
			BirthDate: "31/12/1993",
		}
		err := ValidateUser(user)
		assert.Equal(t, err, fmt.Errorf("error: invalid cpf format. Use the following: xxx.xxx.xxx-xx"))
	})
	t.Run("valid cpf format", func(t *testing.T) {
		user := models.User{
			CPF:       "454.454.454-98",
			Name:      "Someone X",
			BirthDate: "31/12/2093",
		}
		err := ValidateUser(user)
		assert.Equal(t, err, nil)
	})
	t.Run("invalid birth format", func(t *testing.T) {
		user := models.User{
			CPF:       "454.454.454-98",
			Name:      "Someone X",
			BirthDate: "32/12/1993",
		}
		err := ValidateUser(user)
		assert.Equal(t, err, fmt.Errorf("error: invalid birth format. Use the following: dd/mm/yyyy"))
	})
	t.Run("invalid name length", func(t *testing.T) {
		user := models.User{
			CPF:       "454.454.454-98",
			Name:      "",
			BirthDate: "30/12/1993",
		}
		err := ValidateUser(user)
		assert.Equal(t, err, fmt.Errorf("error: please enter a name"))
	})
}

func TestValidateCPF(t *testing.T) {
	t.Run("invalid cpf format", func(t *testing.T) {
		cpf := "454454454984"
		err := ValidateCPF(cpf)
		assert.Equal(t, err, fmt.Errorf("error: invalid cpf format. Use the following: xxx.xxx.xxx-xx"))
	})
	t.Run("valid cpf format", func(t *testing.T) {
		cpf := "454.454.454-98"
		err := ValidateCPF(cpf)
		assert.Equal(t, err, nil)
	})
}

func TestValidateBirth(t *testing.T) {
	t.Run("invalid birth format", func(t *testing.T) {
		birth := "31/13/1993"
		err := ValidateBirth(birth)
		assert.Equal(t, err, fmt.Errorf("error: invalid birth format. Use the following: dd/mm/yyyy"))
	})
	t.Run("invalid birth year", func(t *testing.T) {
		birth := "31/13/2193"
		err := ValidateBirth(birth)
		assert.Equal(t, err, fmt.Errorf("error: invalid birth format. Use the following: dd/mm/yyyy"))
	})
	t.Run("valid birth format", func(t *testing.T) {
		birth := "31/12/1993"
		err := ValidateBirth(birth)
		assert.Equal(t, err, nil)
	})
}

func TestValidateName(t *testing.T) {
	t.Run("invalid name length", func(t *testing.T) {
		name := ""
		err := ValidateName(name)
		assert.Equal(t, err, fmt.Errorf("error: please enter a name"))
	})
	t.Run("valid name length", func(t *testing.T) {
		name := "Someone VS"
		err := ValidateName(name)
		assert.Equal(t, err, nil)
	})
}
