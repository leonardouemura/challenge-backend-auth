package info

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetInfo(t *testing.T) {
	t.Run("must return Dev mode Info", func(t *testing.T) {
		info := GetInfo()

		assert.Equal(t, Info{Version: "Dev", CommitHash: "Dev", BuildTime: "Dev"}, info)
	})
}
func TestGetBuildTime(t *testing.T) {
	t.Run("must return buildTime", func(t *testing.T) {
		i := Info{Version: "1.0.0", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		build := i.GetBuildTime()
		assert.Equal(t, 202207111447, build)
	})
	t.Run("string buildTime must return 0", func(t *testing.T) {
		i := Info{Version: "1.0.0", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "buildTime"}
		build := i.GetBuildTime()
		assert.Equal(t, 0, build)
	})
}

func TestMajor(t *testing.T) {
	t.Run("must return major version", func(t *testing.T) {
		i := Info{Version: "1.2.3", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		major := i.Major()
		assert.Equal(t, 1, major)
	})
	t.Run("string major version", func(t *testing.T) {
		i := Info{Version: "V1.2.3", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		major := i.Major()
		assert.Equal(t, 0, major)
	})
	t.Run("only major version. Valid major version", func(t *testing.T) {
		i := Info{Version: "1", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		major := i.Major()
		assert.Equal(t, 1, major)
	})
	t.Run("emty major version", func(t *testing.T) {
		i := Info{Version: "", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		major := i.Major()
		assert.Equal(t, 0, major)
	})
}
func TestMinor(t *testing.T) {
	t.Run("must return minor version", func(t *testing.T) {
		i := Info{Version: "1.2.3", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		minor := i.Minor()
		assert.Equal(t, 2, minor)
	})
	t.Run("string minor version", func(t *testing.T) {
		i := Info{Version: "1.v2.3", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		minor := i.Minor()
		assert.Equal(t, 0, minor)
	})
	t.Run("only major version", func(t *testing.T) {
		i := Info{Version: "1", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		major := i.Minor()
		assert.Equal(t, 0, major)
	})
	t.Run("no patch version", func(t *testing.T) {
		i := Info{Version: "2.1", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		major := i.Minor()
		assert.Equal(t, 1, major)
	})
}
func TestPatch(t *testing.T) {
	t.Run("must return patch version", func(t *testing.T) {
		i := Info{Version: "1.2.3", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		patch := i.Patch()
		assert.Equal(t, 3, patch)
	})
	t.Run("string patch version", func(t *testing.T) {
		i := Info{Version: "V1.2.3test", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		patch := i.Patch()
		assert.Equal(t, 0, patch)
	})
	t.Run("only major version", func(t *testing.T) {
		i := Info{Version: "1", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		major := i.Patch()
		assert.Equal(t, 0, major)
	})
	t.Run("no patch version", func(t *testing.T) {
		i := Info{Version: "2.1", CommitHash: "1f91c290a7b8a3235e8c2876202fd98f7b35e669", BuildTime: "202207111447"}
		major := i.Patch()
		assert.Equal(t, 0, major)
	})
}

func captureOutput(f func()) string {
	reader, writer, err := os.Pipe()
	if err != nil {
		panic(err)
	}
	stdout := os.Stdout
	stderr := os.Stderr
	defer func() {
		os.Stdout = stdout
		os.Stderr = stderr
		log.SetOutput(os.Stderr)
	}()
	os.Stdout = writer
	os.Stderr = writer
	log.SetOutput(writer)
	out := make(chan string)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		var buf bytes.Buffer
		wg.Done()
		io.Copy(&buf, reader)
		out <- buf.String()
	}()
	wg.Wait()
	f()
	writer.Close()
	return <-out
}

func TestPrintVersion(t *testing.T) {
	t.Run("test console output", func(t *testing.T) {
		re := captureOutput(func() {
			PrintVersion()
		})
		expected := captureOutput(func() {
			fmt.Printf("Version: %s\n", "Dev")
			fmt.Printf("Build Time: %s\n", "Dev")
			fmt.Printf("Commit Hash: %s\n", "Dev")
		})
		assert.Equal(t, expected, re)
	})
}

func TestGetVersion(t *testing.T) {
	t.Run("test console output", func(t *testing.T) {
		info := GetInfo()
		version := info.GetVersion()
		assert.Equal(t, "Dev", version)
	})
}
