package jwt

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/models"

	"github.com/go-redis/redis/v8"
)

// GenerateRefreshToken generates a new refresh token.
func GenerateRefreshToken() (string, error) {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}

// StoreRefreshToken stores the refresh token associated with the user session.
func StoreRefreshToken(ctx context.Context, client *redis.Client, user models.User, refreshToken string) error {
	duration := time.Hour * 2
	sessionToken := models.RefreshToken{Token: refreshToken, CPF: user.CPF, Role: user.Role, ExpiresAt: time.Now().Add(duration)}

	// Convert the RefreshToken struct to JSON.
	jsonSession, err := json.Marshal(sessionToken)
	if err != nil {
		fmt.Println("Error marshaling refresh token to JSON:", err)
		return err
	}

	// Set a key-value pair in Redis with expiration.
	err = client.Set(ctx, user.ID, jsonSession, duration).Err()
	if err != nil {
		fmt.Println("Error setting value with TTL in Redis:", err)
		return err
	}
	fmt.Println("Value with TTL set successfully in Redis.")
	return nil
}

// RetrieveRefreshToken retrieves the refresh token associated with the user session.
func RetrieveRefreshToken(ctx context.Context, client *redis.Client, userID string) (*models.RefreshToken, error) {
	// Get a value from Redis.
	val, err := client.Get(ctx, userID).Result()
	if err == redis.Nil {
		fmt.Println("Key not found in Redis (expired).")
		return nil, fmt.Errorf("%s", "Key not found (expired).")
	} else if err != nil {
		fmt.Println("Error getting value from Redis:", err)
		return nil, fmt.Errorf("error getting key value: %s", err.Error())
	}

	// Unmarshal the JSON string into a RefreshToken struct.
	var refreshToken models.RefreshToken
	if err := json.Unmarshal([]byte(val), &refreshToken); err != nil {
		fmt.Println("Error unmarshaling key value:", err)
		return nil, err
	}

	return &refreshToken, nil
}

// DeleteRefreshToken deletes the refresh token associated with the user session.
func DeleteRefreshToken(ctx context.Context, client *redis.Client, userID string) error {
	// Delete the key associated with the userID.
	err := client.Del(ctx, userID).Err()
	if err != nil {
		fmt.Println("Error deleting value from Redis:", err)
		return fmt.Errorf("error deleting key value: %s", err.Error())
	}
	fmt.Println("Value deleted successfully from Redis.")
	return nil
}

// ValidateRefreshToken validates the refresh token.
func ValidateRefreshToken(ctx context.Context, client *redis.Client, userID string, refreshToken string) (*models.RefreshToken, error) {
	storedRefreshToken, err := RetrieveRefreshToken(ctx, client, userID)
	if err != nil {
		return nil, err
	}
	if storedRefreshToken.Token != refreshToken {
		return nil, errors.New("invalid refresh token")
	}
	if time.Now().After(storedRefreshToken.ExpiresAt) {
		return nil, errors.New("refresh token expired")
	}
	return storedRefreshToken, nil
}

// RefreshJWT generates a new JWT using the refresh token.
func RefreshJWT(ctx context.Context, client *redis.Client, userID string, refreshToken string) (string, error) {
	// Validate the refresh token.
	session, err := ValidateRefreshToken(ctx, client, userID, refreshToken)
	if err != nil {
		return "", err
	}
	// If valid, issue a new JWT for the user.
	token, err := GenerateToken(userID, session.Role)
	if err != nil {
		return "", err
	}
	return token, nil
}
