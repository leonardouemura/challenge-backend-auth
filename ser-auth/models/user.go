package models

// User representa um usuário do sistema.
type User struct {
	BaseModel
	CPF       string `json:"cpf" binding:"required" gorm:"unique;not null"`
	Name      string `json:"nome" binding:"required" gorm:"not null"`
	BirthDate string `json:"data_nascimento" binding:"required" gorm:"not null"`
	Email     string `json:"email" binding:"required" gorm:"unique;not null"`
	Password  string `json:"senha" binding:"required" gorm:"not null"`
	Role      string `json:"tipo" gorm:"default:user"`
	Salt      string `json:"salt"`
}

// Credentials representa as credenciais necessárias para autenticação.
type Credentials struct {
	CPF      string `json:"cpf" binding:"required"`
	Password string `json:"senha" binding:"required"`
}
