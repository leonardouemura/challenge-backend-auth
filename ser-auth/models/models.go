package models

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/config"

	log "github.com/sirupsen/logrus"

	"github.com/go-redis/redis/v8"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// SetupModels setup database
func SetupModels(ctx context.Context, conf *config.Config) (*gorm.DB, *redis.Client) {
	db := SetupPostgres(conf)
	rd := SetupRedis(ctx, conf)
	return db, rd
}

// BaseModel struct to be used in all models
type BaseModel struct {
	ID        string    `json:"id" gorm:"primary_key;type:uuid;default:gen_random_uuid()"`
	CreatedAt time.Time `json:"created_at" example:"2020-01-21T16:33:51.147843-03:00"`
	UpdatedAt time.Time `json:"updated_at" example:"2021-05-21T15:00:49.117789-03:00"`
}

func SetupPostgres(conf *config.Config) *gorm.DB {
	// Load variables
	dbName := conf.DBName
	dbHost := conf.DBHost
	dbPort := conf.DBPort
	dbUser := conf.DBUser
	dbPwd := conf.DBPassword

	// Create postgresql url
	postgresConn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", dbHost, dbPort, dbUser, dbName, dbPwd)

	// Open connection
	db, err := gorm.Open(postgres.Open(postgresConn), &gorm.Config{
		SkipDefaultTransaction: true,
	})
	if err != nil {
		fmt.Println(err)
		panic("Failed to connect to database:")
	}

	// Create tables, if not yet
	db.AutoMigrate(&User{})

	return db
}
func SetupRedis(ctx context.Context, conf *config.Config) *redis.Client {
	dbHost := conf.RedisHost
	dbPort := conf.RedisPort
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", dbHost, dbPort), // Redis server address.
		Password: "",                                   // No password set.
		DB:       0,                                    // Use default DB.
	})

	// Ping the Redis server to check if the connection is established.
	pong, err := client.Ping(ctx).Result()
	if err != nil {
		fmt.Println(err)
		panic("Failed to connect to redis:")
	}
	log.Println("Connected to Redis:", pong)
	return client
}
