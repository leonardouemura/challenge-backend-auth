package models

import "time"

type Refresh struct {
	Access string `json:"access" binding:"required"`
}

// RefreshToken represents the refresh token data.
type RefreshToken struct {
	Token     string
	CPF       string
	Role      string
	ExpiresAt time.Time
}
