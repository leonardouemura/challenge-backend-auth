// Package config configures the Application using .env file, environment variables
// and other necessary configs
package config

// TestConfig configures the application to run int TEST mode
var TestConfig = &Config{
	GinPort:    "5000",
	SecretKey:  "super_secret_key",
	BinaryName: "ser-auth-api",
	LogLevel:   "DEBUG",
	DBName:     "interview_auth",
	DBHost:     "localhost",
	DBPort:     "5432",
	DBUser:     "interview",
	DBPassword: "interview",
	RedisHost:  "localhost",
	RedisPort:  "6379",
	GRPCHost:   "localhost",
	GRPCPort:   "8080",
	AdminEmail: "@gmail.com",
}
