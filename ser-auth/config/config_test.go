// Package config configures the Application using .env file, environment variables
// and other necessary configs
package config

// func TestLoadConfig(t *testing.T) {
// 	t.Run("Test pre-defined TESTING LoadConfig values", func(t *testing.T) {
// 		os.Setenv("ENVIRONMENT", "TESTING")
// 		var dbHost string
// 		var dbUser string
// 		var dbName string
// 		var dbPwd string
// 		var redisHost string
// 		var grpcHost string

// 		_, gitlab_ci := os.LookupEnv("CI_SERVER_HOST")

// 		if gitlab_ci {
// 			dbName = "interview"
// 			dbHost = "postgres"
// 			dbUser = "postgres"
// 			dbPwd = "docker"
// 			redisHost = "redis"
// 			grpcHost = "localhost"
// 		} else {
// 			dbName = "interview_auth"
// 			dbHost = "localhost"
// 			dbUser = "interview"
// 			dbPwd = "interview"
// 			redisHost = "localhost"
// 			grpcHost = "localhost"
// 		}

// 		var ConfAssert = &Config{
// 			"5000",
// 			"super_secret_key",
// 			"ser-auth-api",
// 			"DEBUG",
// 			dbName,
// 			dbHost,
// 			"5432",
// 			dbUser,
// 			dbPwd,
// 			redisHost,
// 			"6379",
// 			grpcHost,
// 			"8080",
// 		}

// 		conf := LoadConfig()

// 		assert.Equal(t, conf, ConfAssert)
// 	})

// 	t.Run("Test pre-defined DEVELOPMENT LoadConfig values", func(t *testing.T) {
// 		os.Setenv("ENVIRONMENT", "DEVELOPMENT")

// 		var dbHost string
// 		var dbUser string
// 		var dbName string
// 		var dbPwd string
// 		var redisHost string
// 		var grpcHost string

// 		_, gitlab_ci := os.LookupEnv("CI_SERVER_HOST")

// 		if gitlab_ci {
// 			dbName = "interview"
// 			dbHost = "postgres"
// 			dbUser = "postgres"
// 			dbPwd = "docker"
// 			redisHost = "redis"
// 			grpcHost = "localhost"
// 		} else {
// 			dbName = "interview_auth"
// 			dbHost = "localhost"
// 			dbUser = "interview"
// 			dbPwd = "interview"
// 			redisHost = "localhost"
// 			grpcHost = "localhost"
// 		}

// 		var ConfAssert = &Config{
// 			"5000",
// 			"super_secret_key",
// 			"ser-auth-api",
// 			"DEBUG",
// 			dbName,
// 			dbHost,
// 			"5432",
// 			dbUser,
// 			dbPwd,
// 			redisHost,
// 			"6379",
// 			grpcHost,
// 			"8080",
// 		}

// 		conf := LoadConfig()

// 		assert.Equal(t, conf, ConfAssert)
// 	})

// 	t.Run("Test pre-defined PRODUCTION LoadConfig values", func(t *testing.T) {
// 		os.Setenv("ENVIRONMENT", "PRODUCTION")

// 		var dbHost string
// 		var dbUser string
// 		var dbName string
// 		var dbPwd string
// 		var redisHost string
// 		var grpcHost string

// 		_, gitlab_ci := os.LookupEnv("CI_SERVER_HOST")

// 		if gitlab_ci {
// 			dbName = "interview"
// 			dbHost = "postgres"
// 			dbUser = "postgres"
// 			dbPwd = "docker"
// 			redisHost = "redis"
// 			grpcHost = "localhost"
// 		} else {
// 			dbName = "interview_auth"
// 			dbHost = "localhost"
// 			dbUser = "interview"
// 			dbPwd = "interview"
// 			redisHost = "localhost"
// 			grpcHost = "localhost"
// 		}

// 		var ConfAssert = &Config{
// 			"5000",
// 			"super_secret_key",
// 			"ser-auth-api",
// 			"INFO",
// 			dbName,
// 			dbHost,
// 			"5432",
// 			dbUser,
// 			dbPwd,
// 			redisHost,
// 			"6379",
// 			grpcHost,
// 			"8080",
// 		}

// 		conf := LoadConfig()

// 		assert.Equal(t, conf, ConfAssert)
// 	})

// 	t.Run("Test pre-defined DEFAULT=PRODUCTION LoadConfig values", func(t *testing.T) {
// 		os.Setenv("ENVIRONMENT", "")

// 		var dbHost string
// 		var dbUser string
// 		var dbName string
// 		var dbPwd string
// 		var redisHost string
// 		var grpcHost string

// 		_, gitlab_ci := os.LookupEnv("CI_SERVER_HOST")

// 		if gitlab_ci {
// 			dbName = "interview"
// 			dbHost = "postgres"
// 			dbUser = "postgres"
// 			dbPwd = "docker"
// 			redisHost = "redis"
// 			grpcHost = "localhost"
// 		} else {
// 			dbName = "interview_auth"
// 			dbHost = "localhost"
// 			dbUser = "interview"
// 			dbPwd = "interview"
// 			redisHost = "localhost"
// 			grpcHost = "localhost"
// 		}

// 		var ConfAssert = &Config{
// 			"5000",
// 			"super_secret_key",
// 			"ser-auth-api",
// 			"INFO",
// 			dbName,
// 			dbHost,
// 			"5432",
// 			dbUser,
// 			dbPwd,
// 			redisHost,
// 			"6379",
// 			grpcHost,
// 			"8080",
// 		}

// 		conf := LoadConfig()

// 		assert.Equal(t, conf, ConfAssert)
// 	})

// 	t.Run("using LoadConfig and .env, validate enviroment variables", func(t *testing.T) {
// 		os.Setenv("ENVIRONMENT", "")
// 		loadDotEnv("environment_test")

// 		var dbName string
// 		var dbHost string
// 		var dbPort string
// 		var dbUser string
// 		var dbPwd string
// 		var redisHost string
// 		var grpcHost string

// 		_, gitlab_ci := os.LookupEnv("CI_SERVER_HOST")

// 		if gitlab_ci {
// 			dbName = "interview"
// 			dbHost = "postgres"
// 			dbUser = "postgres"
// 			dbPwd = "docker"
// 			redisHost = "redis"
// 			grpcHost = "localhost"
// 		} else {
// 			dbName = "mydb"
// 			dbHost = "myhost"
// 			dbPort = "dbport"
// 			dbUser = "dbuser"
// 			dbPwd = "dbpass"
// 			redisHost = "redishost"
// 			grpcHost = "grpchost"
// 		}

// 		var ConfAssert = &Config{
// 			"5000",
// 			"mysecretkey",
// 			"mybin",
// 			"INFO",
// 			dbName,
// 			dbHost,
// 			dbPort,
// 			dbUser,
// 			dbPwd,
// 			redisHost,
// 			"redisport",
// 			grpcHost,
// 			"grpcport",
// 		}

// 		conf := LoadConfig()

// 		assert.Equal(t, conf, ConfAssert)
// 	})
// }

// func TestLoadDotEnv(t *testing.T) {
// 	t.Run("get .env and validate enviroment variables", func(t *testing.T) {
// 		loadDotEnv("environment_test")

// 		var dbName string
// 		var dbHost string
// 		var dbPort string
// 		var dbUser string
// 		var dbPwd string
// 		var redisHost string
// 		var grpcHost string

// 		_, gitlab_ci := os.LookupEnv("CI_SERVER_HOST")

// 		if gitlab_ci {
// 			dbName = "interview"
// 			dbHost = "postgres"
// 			dbUser = "postgres"
// 			dbPwd = "docker"
// 			redisHost = "redis"
// 			grpcHost = "localhost"
// 		} else {
// 			dbName = "mydb"
// 			dbHost = "myhost"
// 			dbPort = "dbport"
// 			dbUser = "dbuser"
// 			dbPwd = "dbpass"
// 			redisHost = "redishost"
// 			grpcHost = "grpchost"
// 		}

// 		assert.Equal(t, os.Getenv("LOG_LEVEL"), "INFO")
// 		assert.Equal(t, os.Getenv("POSTGRES_NAME"), dbName)
// 		assert.Equal(t, os.Getenv("POSTGRES_HOST"), dbHost)
// 		assert.Equal(t, os.Getenv("POSTGRES_PORT"), dbPort)
// 		assert.Equal(t, os.Getenv("POSTGRES_USER"), dbUser)
// 		assert.Equal(t, os.Getenv("POSTGRES_PWD"), dbPwd)
// 		assert.Equal(t, os.Getenv("PORT"), "5000")
// 		assert.Equal(t, os.Getenv("REDIS_HOST"), redisHost)
// 		assert.Equal(t, os.Getenv("GRPC_HOST"), grpcHost)

// 		assert.Equal(t, os.Getenv("SECRET_KEY"), "mysecretkey")
// 		assert.Equal(t, os.Getenv("BINARY_NAME"), "mybin")
// 	})
// }
