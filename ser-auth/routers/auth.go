package routers

import (
	"net/http"
	"strings"
	"time"

	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/jwt"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/models"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
)

func Authentication(c *gin.Context) {
	rd := c.MustGet("rd").(*redis.Client)

	reqToken := c.Request.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer ")
	token := splitToken[1]
	claims, err := jwt.ParseToken(token)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
		return
	}

	// valida se a sessão
	session, err := jwt.RetrieveRefreshToken(c, rd, claims.Id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}
	if session.Role != claims.Role || time.Now().After(session.ExpiresAt) {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "invalid session. Please login again"})
		return
	}

	c.Set("claims", claims)

	// Continue with the chain to handler
	c.Next()
}

func Login(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	rd := c.MustGet("rd").(*redis.Client)

	var input models.Credentials
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var user models.User
	tx := db.Model(&models.User{}).Where("cpf = ?", input.CPF).Find(&user)
	tx.Statement.RaiseErrorOnNotFound = true

	encodedPassword, err := EncodePassword(input.Password, user.Salt)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "failed to encrypt password"})
	}
	if user.Password != encodedPassword {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid credentials"})
		return
	}

	// Gera token JWT
	token, err := jwt.GenerateToken(user.ID, user.Role)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Gera sessão
	sessionKey, err := jwt.GenerateRefreshToken()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := jwt.StoreRefreshToken(c, rd, user, sessionKey); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"token": token, "access": sessionKey})
}

func Logout(c *gin.Context) {
	rd := c.MustGet("rd").(*redis.Client)
	claims := c.MustGet("claims").(*jwt.Claims)

	if err := jwt.DeleteRefreshToken(c, rd, claims.Id); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "logout successful"})
}

func CheckToken(c *gin.Context) {
	rd := c.MustGet("rd").(*redis.Client)

	reqToken := c.Request.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer ")
	token := splitToken[1]
	claims, err := jwt.ParseToken(token)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// valida se a sessão
	session, err := jwt.RetrieveRefreshToken(c, rd, claims.Id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if session.Role != claims.Role || time.Now().After(session.ExpiresAt) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid session. Please login again"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"token": claims})
}

func RefreshToken(c *gin.Context) {
	rd := c.MustGet("rd").(*redis.Client)

	userID := c.Param("id")
	if len(userID) == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "inform a user id"})
		return
	}

	var input models.Refresh
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	newToken, err := jwt.RefreshJWT(c, rd, userID, input.Access)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"token": newToken})
}
