package routers

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"net/http"
	"strings"

	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/config"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/models"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-auth/validators"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/pbkdf2"
	"gorm.io/gorm"
)

func isEmailAdmin(email string) bool {
	conf := config.LoadConfig()
	return strings.HasSuffix(email, conf.AdminEmail)
}

// GetRandomString generate random string by specify chars.
// source: https://github.com/gogits/gogs/blob/9ee80e3e5426821f03a4e99fad34418f5c736413/modules/base/tool.go#L58
func GetRandomString(n int, alphabets ...byte) (string, error) {
	const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	var bytes = make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}

	for i, b := range bytes {
		if len(alphabets) == 0 {
			bytes[i] = alphanum[b%byte(len(alphanum))]
		} else {
			bytes[i] = alphabets[b%byte(len(alphabets))]
		}
	}
	return string(bytes), nil
}

// EncodePassword encodes a password using PBKDF2.
func EncodePassword(password string, salt string) (string, error) {
	newPasswd := pbkdf2.Key([]byte(password), []byte(salt), 10000, 50, sha256.New)
	return hex.EncodeToString(newPasswd), nil
}

func CreateUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var input models.User
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := validators.ValidateUser(input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "validation error! " + err.Error()})
		return
	}
	input.Role = "user"
	if isEmailAdmin(input.Email) {
		input.Role = "admin"
	}

	// encode password
	salt, err := GetRandomString(10)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "encryption error! " + err.Error()})
		return
	}
	input.Salt = salt
	encodedPassword, err := EncodePassword(input.Password, input.Salt)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "failed to encrypt password"})
	}
	input.Password = encodedPassword

	// Create user on database
	if err := db.Create(&input).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusCreated, input)
}
