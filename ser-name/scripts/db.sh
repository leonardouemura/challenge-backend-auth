#!/usr/bin/env bash
set -e

sudo -u postgres psql -c 'CREATE DATABASE interview_name;' || true
sudo -u postgres psql -c "CREATE ROLE interview WITH SUPERUSER LOGIN PASSWORD 'interview';" || true
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE interview_name TO interview;"