package models

import "time"

// Debt representa uma dívida do usuário.
type Debt struct {
	BaseModel
	Value   float64   `json:"valor" binding:"required" gorm:"not null"`
	DueDate time.Time `json:"data_vencimento" binding:"required" gorm:"not null"`
	CPF     string    `json:"cpf" binding:"required" gorm:"not null"`
}
