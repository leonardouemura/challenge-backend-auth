package filters

import (
	"gorm.io/gorm"
)

type DebtFilter struct {
	Value float64 `form:"valor"`
	IDs   []uint  `form:"ids"`
}

func (filter *DebtFilter) Filter(query *gorm.DB) *gorm.DB {
	if filter.Value != 0 {
		query = query.Where("value = ?", filter.Value)
	}
	if len(filter.IDs) > 0 {
		query = query.Where("id IN ?", filter.IDs)
	}
	return query
}
