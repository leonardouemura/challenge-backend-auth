package validators

import (
	"fmt"
	"regexp"
	"time"

	"gitlab.com/leonardouemura/challenge-backend-auth/ser-name/models"
)

func ValidateDebt(debt models.Debt) error {
	if err := ValidateCPF(debt.CPF); err != nil {
		return err
	}
	if err := ValidateDueDate(debt.DueDate); err != nil {
		return err
	}
	if err := ValidateValue(debt.Value); err != nil {
		return err
	}
	return nil
}

func ValidateCPF(cpf string) error {
	pattern := `^\d{3}\.\d{3}\.\d{3}-\d{2}$`
	if match, _ := regexp.MatchString(pattern, cpf); match {
		return nil
	} else {
		return fmt.Errorf("error: invalid cpf format. Use the following: xxx.xxx.xxx-xx")
	}
}

func ValidateDueDate(dueDate time.Time) error {
	// TODO: verificar se existe alguma limitação de data
	return nil
}

func ValidateValue(value float64) error {
	if value <= 0 {
		return fmt.Errorf("error: digite um número maior que zero")
	}
	return nil
}
