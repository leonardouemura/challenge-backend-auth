package routers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	pb "gitlab.com/leonardouemura/challenge-backend-auth/rpc_auth"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-name/filters"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-name/models"
	"gitlab.com/leonardouemura/challenge-backend-auth/ser-name/validators"
	"gorm.io/gorm"
)

func CreateDebt(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	validation := c.MustGet("validation").(*pb.ValidateTokenResponse)

	if validation.Role != "admin" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "user role without write privileges"})
		return
	}
	// Validate input
	var input models.Debt
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := validators.ValidateDebt(input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Validation error! " + err.Error()})
		return
	}

	// Create debt on database
	db.Create(&input)
	c.JSON(http.StatusCreated, input)
}

func ListDebt(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	validation := c.MustGet("validation").(*pb.ValidateTokenResponse)

	var debtFilter filters.DebtFilter
	if err := c.ShouldBindQuery(&debtFilter); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var debts []models.Debt
	query := db.Model(&models.Debt{}).Where("cpf = ?", validation.CPF)
	debtFilter.Filter(query).Find(&debts)
	// debtFilter.Filter(query).Order("name ASC").Find(&debts) //TODO: order by due_date

	// Set response
	c.JSON(http.StatusOK, gin.H{"items": debts})
}
