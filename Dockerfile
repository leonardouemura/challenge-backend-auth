FROM golang:latest

# Set destination for COPY
WORKDIR /app

# Download Go modules
COPY . .
# # Build
RUN mkdir ./build/
RUN CGO_ENABLED=0 GOOS=linux go build -o ./build/ser-auth-api ./ser-auth/cmd/api/main.go
RUN CGO_ENABLED=0 GOOS=linux go build -o ./build/ser-auth-grpc ./ser-auth/cmd/grpc/main.go
RUN CGO_ENABLED=0 GOOS=linux go build -o ./build/ser-name-api ./ser-name/main.go 
RUN CGO_ENABLED=0 GOOS=linux go build -o ./build/ser-score-api ./ser-score/main.go