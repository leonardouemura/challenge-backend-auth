create database interview_auth owner interview;
create database interview_name owner interview;

CREATE ROLE interview WITH SUPERUSER LOGIN PASSWORD 'interview';
GRANT ALL PRIVILEGES ON DATABASE interview_auth TO interview;
GRANT ALL PRIVILEGES ON DATABASE interview_name TO interview;